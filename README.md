# Oh my Vim

- `git submodule add <plugins git repo> plugins/start`: include new plugins
- `git submodule update --remote --merge`: update the plugins

### ALE
- `flake8`, `isort`, `black` need to be installed on the working machine for linter and fixer to work.
- `eslint` is needed to be configured for linter working for JS code
- `yamlint` and `ansible-lint` will need to be installed for yaml linting.


### fzf
- `fzf` and `ripgrep` need to be installed on the working machine


### YouCompleteMe
- `brew install cmake node npm ts` need to be installed.
- `ln -s /usr/local/opt/macvim/bin/mvim /usr/local/bin/mvim`: make sure to soft link the mvim on Mac, as YCM does not work with Vim because of ptyon3 is not supported.
- `third_party/ycmd/third_party/tern_runtime/node_modules`: inside of the plugin folder, make sure to delete this folder to enable TSServer engine to work.
`./install.py --ts-completer`: to complie the plugin.

