" enter the current millenium
set nocompatible

" enable syntax and plugins for (netrw)
syntax enable
filetype plugin on
" Set font for MacVim
set guifont=Menlo\ Regular:h18

" Set folding method
set foldmethod=syntax
set nofoldenable

set number
set rnu

set showmatch
set cursorline

set tabstop=4 softtabstop=4
set shiftwidth=4
set expandtab
set smartindent
set smarttab

set smartcase
set ignorecase
set incsearch
"set hlsearch

" Use autodir for creating new files
set autochdir

" Highligt trailing spaces
highlight ExtraWhitespace ctermbg=red guibg=red
match ExtraWhitespace /\s\+$/
autocmd BufWinEnter * match ExtraWhitespace /\s\+$/
autocmd InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
autocmd InsertLeave * match ExtraWhitespace /\s\+$/
autocmd BufWinLeave * call clearmatches()

set noswapfile
set backspace=indent,eol,start
" :W save file in read only mode
command W w !sudo tee % > /dev/null

" set spaces and tabs with characters, may conflicts with colorscheme
"set listchars=eol:$,nbsp:_,tab:>-,trail:~,extends:>,precedes:<

" NERDTree show hidden file
let NERDTreeShowHidden=1
" NERDTree toggle using <F6>
nmap <F8> :NERDTreeToggle<CR>

" Set ColorScheme
"let g:solarized_termcolors=256
set background=dark
colorscheme gruvbox


" Set current working directory
autocmd BufEnter * lcd %:p:h
" Start NERDTree and leave the cursor in it.
autocmd VimEnter * NERDTree | wincmd p
autocmd VimEnter * call lightline#update()
set laststatus=2

" Exit Vim if NERDTree is the only window left.
autocmd BufEnter * if tabpagenr('$') == 1 && winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() |
    \ quit | endif

" Set GitGutter update time
set updatetime=100

" Update GitGutter highlight color
highlight GitGutterAdd    guifg=#009900 ctermfg=Green
highlight GitGutterChange guifg=#bbbb00 ctermfg=Yellow
highlight GitGutterDelete guifg=#ff2222 ctermfg=Red

" Get git status from GitGutter
function! GitStatus()
  let [a,m,r] = GitGutterGetHunkSummary()
  return printf('+%d ~%d -%d', a, m, r)
endfunction
set statusline+=%{GitStatus()}

" Update LightLine Status bar
let g:lightline = {
      \ 'colorscheme': 'gruvbox',
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'gitbranch', 'readonly', 'filename', 'modified' ] ]
      \ },
      \ 'component_function': {
      \   'gitbranch': 'FugitiveHead',
      \   'filename': 'LightlineFilename',
      \ },
      \ }

function! LightlineFilename()
  let root = fnamemodify(get(b:, 'git_dir'), ':h')
  let path = expand('%:p')
  if path[:len(root)-1] ==# root
    return path[len(root)+1:]
  endif
  return expand('%')
endfunction

" ALE
let g:ale_linters = {
    \  'python': ['flake8'],
    \ }
let g:ale_fixers = {
    \  '*': ['remove_trailing_lines', 'trim_whitespace'],
    \ 'python': ['black', 'isort']
    \ }

let g:ale_python_flake8_options = '--max-line-length=88'
" Fuzzy File Finder
"
" Push fzf windown on the bottom of the window
let g:fzf_layout = { 'down':  '40%'}

" Rg command with preview window and only match file contents
command! -bang -nargs=* Rg
  \ call fzf#vim#grep(
  \   'rg --glob "!node_modules/" --glob "!.git/" --column --line-number --no-heading --color=always --smart-case '.shellescape(<q-args>), 1,
  \   fzf#vim#with_preview({'options': '--delimiter : --nth 4..'}, 'right:40%'), <bang>0)

" Fzf search without node_modules and .git folder
command! -bang -nargs=*  All
  \ call fzf#run(fzf#wrap({'source': 'rg --files --hidden --no-ignore-vcs -g "!node_modules/" -g "!.git/"', 'down': '40%', 'options': '--expect=ctrl-t,ctrl-x,ctrl-v --multi --reverse' }))

" Auto complete things like brackets and quotes
inoremap { {}<Esc>ha
inoremap ( ()<Esc>ha
inoremap [ []<Esc>ha
inoremap " ""<Esc>ha
inoremap ' ''<Esc>ha
inoremap ` ``<Esc>ha
